﻿using UnityEngine;
using System.Collections;

public abstract class Dot : MonoBehaviour {

	private float lifeTime;
	private Vector2 position;

	void Start () {
		lifeTime = Random.Range (10, 50) / 10.0f;
		float scale = Random.Range (5, 15) / 10.0f;
		transform.localScale = new Vector3 (scale, scale, 1);
	}

	void Update () {
		Vector3 screenPosition = Camera.main.WorldToScreenPoint(transform.position);
		position = new Vector2(screenPosition.x - 62f, Screen.height - screenPosition.y - 10f);

		if (lifeTime > 0) {
			lifeTime -= Time.deltaTime;
			lifeTime= 10;
		}
		else {
			disappear ();
		}
	}

	protected virtual void OnMouseDown() {
		reset ();
	}

	protected virtual void disappear() {
		reset ();
	}

	protected void reset() {
		Destroy(gameObject);
		Game.Instance.createDot();
		if(gameObject!=NULL)
		return;
	}

	void OnGUI()
	{
		if (lifeTime < 0)
			return;

		Rect rect = new Rect(position.x, position.y, 120f, 20f);

		GUIStyle label = new GUIStyle(GUI.skin.label);
		label.alignment = TextAnchor.MiddleCenter;

		GUI.Label(rect, lifeTime.ToString("0.00"), label);
	}
}
